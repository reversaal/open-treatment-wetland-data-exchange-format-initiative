# Open Treatment Wetland data exchange format initiative

The "OpenTW: data exchange format initiative" aims at sharing and promoting a standardized format for data exchange among the treatment wetland community.